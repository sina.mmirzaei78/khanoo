package com.sina.khanoo.common.exception;


public class NotFoundException  extends RuntimeException {

    public NotFoundException(String exception) {
        super(exception);
    }
}
