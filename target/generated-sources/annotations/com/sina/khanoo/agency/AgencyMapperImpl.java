package com.sina.khanoo.agency;

import com.sina.khanoo.agency_manager.AgencyManagerMapper;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-20T01:41:08+0430",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
@Component
public class AgencyMapperImpl implements AgencyMapper {

    @Autowired
    private AgencyManagerMapper agencyManagerMapper;

    @Override
    public Agency toAgency(AgencyRequestDTO agencyRequestDTO) {
        if ( agencyRequestDTO == null ) {
            return null;
        }

        Agency agency = new Agency();

        agency.setAgencyManager( agencyManagerMapper.toAgencyManager( agencyRequestDTO.getAgencyManagerRequestDTO() ) );
        agency.setId( agencyRequestDTO.getId() );
        agency.setTitle( agencyRequestDTO.getTitle() );
        agency.setAddress( agencyRequestDTO.getAddress() );
        agency.setTeleNumber( agencyRequestDTO.getTeleNumber() );
        agency.setDescription( agencyRequestDTO.getDescription() );
        agency.setActivityDomain( agencyRequestDTO.getActivityDomain() );

        return agency;
    }

    @Override
    public List<Agency> toAgencyList(List<AgencyRequestDTO> agencyRequestDTOList) {
        if ( agencyRequestDTOList == null ) {
            return null;
        }

        List<Agency> list = new ArrayList<Agency>( agencyRequestDTOList.size() );
        for ( AgencyRequestDTO agencyRequestDTO : agencyRequestDTOList ) {
            list.add( toAgency( agencyRequestDTO ) );
        }

        return list;
    }

    @Override
    public AgencyResponseDTO toAgencyResponseDTO(Agency agency) {
        if ( agency == null ) {
            return null;
        }

        AgencyResponseDTO agencyResponseDTO = new AgencyResponseDTO();

        agencyResponseDTO.setAgencyManagerResponseDTO( agencyManagerMapper.toAgencyManagerResponseDTO( agency.getAgencyManager() ) );
        agencyResponseDTO.setId( agency.getId() );
        agencyResponseDTO.setVersion( agency.getVersion() );
        agencyResponseDTO.setCreatedDate( agency.getCreatedDate() );
        agencyResponseDTO.setLastModifiedDate( agency.getLastModifiedDate() );
        agencyResponseDTO.setTitle( agency.getTitle() );
        agencyResponseDTO.setAddress( agency.getAddress() );
        agencyResponseDTO.setTeleNumber( agency.getTeleNumber() );
        agencyResponseDTO.setDescription( agency.getDescription() );
        agencyResponseDTO.setActivityDomain( agency.getActivityDomain() );
        agencyResponseDTO.setIsActive( agency.getIsActive() );

        return agencyResponseDTO;
    }

    @Override
    public List<AgencyResponseDTO> toAgencyResponseDTOList(List<Agency> agencyList) {
        if ( agencyList == null ) {
            return null;
        }

        List<AgencyResponseDTO> list = new ArrayList<AgencyResponseDTO>( agencyList.size() );
        for ( Agency agency : agencyList ) {
            list.add( toAgencyResponseDTO( agency ) );
        }

        return list;
    }
}
