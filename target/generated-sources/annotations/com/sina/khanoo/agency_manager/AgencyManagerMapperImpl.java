package com.sina.khanoo.agency_manager;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-20T01:41:08+0430",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
@Component
public class AgencyManagerMapperImpl implements AgencyManagerMapper {

    @Override
    public AgencyManager toAgencyManager(AgencyManagerRequestDTO agencyManagerRequestDTO) {
        if ( agencyManagerRequestDTO == null ) {
            return null;
        }

        AgencyManager agencyManager = new AgencyManager();

        agencyManager.setId( agencyManagerRequestDTO.getId() );
        agencyManager.setName( agencyManagerRequestDTO.getName() );

        return agencyManager;
    }

    @Override
    public List<AgencyManager> toAgencyManagerList(List<AgencyManagerRequestDTO> agencyManagerRequestDTOList) {
        if ( agencyManagerRequestDTOList == null ) {
            return null;
        }

        List<AgencyManager> list = new ArrayList<AgencyManager>( agencyManagerRequestDTOList.size() );
        for ( AgencyManagerRequestDTO agencyManagerRequestDTO : agencyManagerRequestDTOList ) {
            list.add( toAgencyManager( agencyManagerRequestDTO ) );
        }

        return list;
    }

    @Override
    public AgencyManagerResponseDTO toAgencyManagerResponseDTO(AgencyManager agencyManager) {
        if ( agencyManager == null ) {
            return null;
        }

        AgencyManagerResponseDTO agencyManagerResponseDTO = new AgencyManagerResponseDTO();

        agencyManagerResponseDTO.setId( agencyManager.getId() );
        agencyManagerResponseDTO.setVersion( agencyManager.getVersion() );
        agencyManagerResponseDTO.setCreatedDate( agencyManager.getCreatedDate() );
        agencyManagerResponseDTO.setLastModifiedDate( agencyManager.getLastModifiedDate() );
        agencyManagerResponseDTO.setName( agencyManager.getName() );
        agencyManagerResponseDTO.setIsActive( agencyManager.getIsActive() );

        return agencyManagerResponseDTO;
    }

    @Override
    public List<AgencyManagerResponseDTO> toAgencyManagerResponseDTOList(List<AgencyManager> agencyManagerList) {
        if ( agencyManagerList == null ) {
            return null;
        }

        List<AgencyManagerResponseDTO> list = new ArrayList<AgencyManagerResponseDTO>( agencyManagerList.size() );
        for ( AgencyManager agencyManager : agencyManagerList ) {
            list.add( toAgencyManagerResponseDTO( agencyManager ) );
        }

        return list;
    }
}
