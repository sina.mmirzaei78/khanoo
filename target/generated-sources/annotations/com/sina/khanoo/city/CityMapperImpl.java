package com.sina.khanoo.city;

import com.sina.khanoo.zone.ZoneMapper;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-20T01:41:08+0430",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
@Component
public class CityMapperImpl implements CityMapper {

    @Autowired
    private ZoneMapper zoneMapper;

    @Override
    public City toCity(CityRequestDTO cityRequestDTO) {
        if ( cityRequestDTO == null ) {
            return null;
        }

        City city = new City();

        city.setId( cityRequestDTO.getId() );
        city.setName( cityRequestDTO.getName() );

        return city;
    }

    @Override
    public List<City> toCityList(List<CityRequestDTO> cityRequestDTOList) {
        if ( cityRequestDTOList == null ) {
            return null;
        }

        List<City> list = new ArrayList<City>( cityRequestDTOList.size() );
        for ( CityRequestDTO cityRequestDTO : cityRequestDTOList ) {
            list.add( toCity( cityRequestDTO ) );
        }

        return list;
    }

    @Override
    public CityResponseDTO toCityResponseDTO(City city) {
        if ( city == null ) {
            return null;
        }

        CityResponseDTO cityResponseDTO = new CityResponseDTO();

        cityResponseDTO.setZoneResponseDTOList( zoneMapper.toZoneResponseDTOList( city.getZoneList() ) );
        cityResponseDTO.setId( city.getId() );
        cityResponseDTO.setVersion( city.getVersion() );
        cityResponseDTO.setCreatedDate( city.getCreatedDate() );
        cityResponseDTO.setLastModifiedDate( city.getLastModifiedDate() );
        cityResponseDTO.setName( city.getName() );
        cityResponseDTO.setIsActive( city.getIsActive() );

        return cityResponseDTO;
    }

    @Override
    public List<CityResponseDTO> toCityResponseDTOList(List<City> cityList) {
        if ( cityList == null ) {
            return null;
        }

        List<CityResponseDTO> list = new ArrayList<CityResponseDTO>( cityList.size() );
        for ( City city : cityList ) {
            list.add( toCityResponseDTO( city ) );
        }

        return list;
    }
}
