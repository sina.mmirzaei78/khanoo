package com.sina.khanoo.house;

import com.sina.khanoo.agency.AgencyMapper;
import com.sina.khanoo.house_feature.HouseFeature;
import com.sina.khanoo.house_feature.HouseFeatureMapper;
import com.sina.khanoo.house_feature.HouseFeatureRequestDTO;
import com.sina.khanoo.zone.ZoneMapper;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-20T01:41:08+0430",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
@Component
public class HouseMapperImpl implements HouseMapper {

    @Autowired
    private ZoneMapper zoneMapper;
    @Autowired
    private HouseFeatureMapper houseFeatureMapper;
    @Autowired
    private AgencyMapper agencyMapper;

    @Override
    public House toHouse(HouseRequestDTO houseRequestDTO) {
        if ( houseRequestDTO == null ) {
            return null;
        }

        House house = new House();

        house.setZone( zoneMapper.toZone( houseRequestDTO.getZoneRequestDTO() ) );
        house.setAgency( agencyMapper.toAgency( houseRequestDTO.getAgencyRequestDTO() ) );
        house.setHouseFeatures( houseFeatureRequestDTOListToHouseFeatureList( houseRequestDTO.getHouseFeatureRequestDTOList() ) );
        house.setLocation( convertLocationDTOToLocation( houseRequestDTO.getLocation() ) );
        house.setId( houseRequestDTO.getId() );
        house.setMeters( houseRequestDTO.getMeters() );
        house.setAddress( houseRequestDTO.getAddress() );
        house.setRoomCount( houseRequestDTO.getRoomCount() );
        house.setWichFloor( houseRequestDTO.getWichFloor() );
        house.setSalePrice( houseRequestDTO.getSalePrice() );
        house.setRentPrice( houseRequestDTO.getRentPrice() );
        house.setMortgagePrice( houseRequestDTO.getMortgagePrice() );
        house.setHouseUsage( houseRequestDTO.getHouseUsage() );
        house.setHousePosition( houseRequestDTO.getHousePosition() );
        house.setHouseStatus( houseRequestDTO.getHouseStatus() );
        house.setHouseType( houseRequestDTO.getHouseType() );

        return house;
    }

    @Override
    public List<House> toHouseList(List<HouseRequestDTO> houseRequestDTOList) {
        if ( houseRequestDTOList == null ) {
            return null;
        }

        List<House> list = new ArrayList<House>( houseRequestDTOList.size() );
        for ( HouseRequestDTO houseRequestDTO : houseRequestDTOList ) {
            list.add( toHouse( houseRequestDTO ) );
        }

        return list;
    }

    @Override
    public HouseResponseDTO toHouseResponseDTO(House house) {
        if ( house == null ) {
            return null;
        }

        HouseResponseDTO houseResponseDTO = new HouseResponseDTO();

        houseResponseDTO.setZoneResponseDTO( zoneMapper.toZoneResponseDTO( house.getZone() ) );
        houseResponseDTO.setAgencyResponseDTO( agencyMapper.toAgencyResponseDTO( house.getAgency() ) );
        houseResponseDTO.setHouseFeatureResponseDTOList( houseFeatureMapper.toHouseFeatureResponseDTOList( house.getHouseFeatures() ) );
        houseResponseDTO.setLocation( convertLocationToLocationDTO( house.getLocation() ) );
        houseResponseDTO.setId( house.getId() );
        houseResponseDTO.setVersion( house.getVersion() );
        houseResponseDTO.setCreatedDate( house.getCreatedDate() );
        houseResponseDTO.setLastModifiedDate( house.getLastModifiedDate() );
        houseResponseDTO.setMeters( house.getMeters() );
        houseResponseDTO.setAddress( house.getAddress() );
        houseResponseDTO.setRoomCount( house.getRoomCount() );
        houseResponseDTO.setWichFloor( house.getWichFloor() );
        houseResponseDTO.setSalePrice( house.getSalePrice() );
        houseResponseDTO.setRentPrice( house.getRentPrice() );
        houseResponseDTO.setMortgagePrice( house.getMortgagePrice() );
        houseResponseDTO.setHouseUsage( house.getHouseUsage() );
        houseResponseDTO.setHousePosition( house.getHousePosition() );
        houseResponseDTO.setHouseStatus( house.getHouseStatus() );
        houseResponseDTO.setHouseType( house.getHouseType() );
        houseResponseDTO.setIsActive( house.getIsActive() );

        return houseResponseDTO;
    }

    @Override
    public List<HouseResponseDTO> toHouseResponseDTOList(List<House> houseList) {
        if ( houseList == null ) {
            return null;
        }

        List<HouseResponseDTO> list = new ArrayList<HouseResponseDTO>( houseList.size() );
        for ( House house : houseList ) {
            list.add( toHouseResponseDTO( house ) );
        }

        return list;
    }

    protected List<HouseFeature> houseFeatureRequestDTOListToHouseFeatureList(List<HouseFeatureRequestDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<HouseFeature> list1 = new ArrayList<HouseFeature>( list.size() );
        for ( HouseFeatureRequestDTO houseFeatureRequestDTO : list ) {
            list1.add( houseFeatureMapper.toHouseFeature( houseFeatureRequestDTO ) );
        }

        return list1;
    }
}
