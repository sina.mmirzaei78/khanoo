package com.sina.khanoo.house_feature;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-20T01:41:08+0430",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
@Component
public class HouseFeatureMapperImpl implements HouseFeatureMapper {

    @Override
    public HouseFeature toHouseFeature(HouseFeatureRequestDTO houseFeatureRequestDTO) {
        if ( houseFeatureRequestDTO == null ) {
            return null;
        }

        HouseFeature houseFeature = new HouseFeature();

        houseFeature.setId( houseFeatureRequestDTO.getId() );
        houseFeature.setHouseFeatureEnum( houseFeatureRequestDTO.getHouseFeatureEnum() );

        return houseFeature;
    }

    @Override
    public List<HouseFeature> toHouseFeatureList(List<HouseFeature> houseFeatureList) {
        if ( houseFeatureList == null ) {
            return null;
        }

        List<HouseFeature> list = new ArrayList<HouseFeature>( houseFeatureList.size() );
        for ( HouseFeature houseFeature : houseFeatureList ) {
            list.add( houseFeature );
        }

        return list;
    }

    @Override
    public HouseFeatureResponseDTO toHouseFeatureResponseDTO(HouseFeature houseFeature) {
        if ( houseFeature == null ) {
            return null;
        }

        HouseFeatureResponseDTO houseFeatureResponseDTO = new HouseFeatureResponseDTO();

        houseFeatureResponseDTO.setId( houseFeature.getId() );
        houseFeatureResponseDTO.setVersion( houseFeature.getVersion() );
        houseFeatureResponseDTO.setCreatedDate( houseFeature.getCreatedDate() );
        houseFeatureResponseDTO.setLastModifiedDate( houseFeature.getLastModifiedDate() );
        houseFeatureResponseDTO.setHouseFeatureEnum( houseFeature.getHouseFeatureEnum() );
        houseFeatureResponseDTO.setIsActive( houseFeature.getIsActive() );

        return houseFeatureResponseDTO;
    }

    @Override
    public List<HouseFeatureResponseDTO> toHouseFeatureResponseDTOList(List<HouseFeature> houseFeatureList) {
        if ( houseFeatureList == null ) {
            return null;
        }

        List<HouseFeatureResponseDTO> list = new ArrayList<HouseFeatureResponseDTO>( houseFeatureList.size() );
        for ( HouseFeature houseFeature : houseFeatureList ) {
            list.add( toHouseFeatureResponseDTO( houseFeature ) );
        }

        return list;
    }
}
