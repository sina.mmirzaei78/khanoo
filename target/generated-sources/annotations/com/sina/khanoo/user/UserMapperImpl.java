package com.sina.khanoo.user;

import com.sina.khanoo.user.role.Role;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-20T01:41:08+0430",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public AppUser toUser(UserRequestDTO userRequestDTO) {
        if ( userRequestDTO == null ) {
            return null;
        }

        AppUser appUser = new AppUser();

        appUser.setId( userRequestDTO.getId() );
        appUser.setFullname( userRequestDTO.getFullname() );
        appUser.setUsername( userRequestDTO.getUsername() );
        appUser.setPassword( userRequestDTO.getPassword() );

        return appUser;
    }

    @Override
    public List<AppUser> toUserList(List<UserRequestDTO> userRequestDTOList) {
        if ( userRequestDTOList == null ) {
            return null;
        }

        List<AppUser> list = new ArrayList<AppUser>( userRequestDTOList.size() );
        for ( UserRequestDTO userRequestDTO : userRequestDTOList ) {
            list.add( toUser( userRequestDTO ) );
        }

        return list;
    }

    @Override
    public UserResponseDTO toUserResponseDTO(AppUser appUser) {
        if ( appUser == null ) {
            return null;
        }

        UserResponseDTO userResponseDTO = new UserResponseDTO();

        userResponseDTO.setId( appUser.getId() );
        userResponseDTO.setVersion( appUser.getVersion() );
        userResponseDTO.setCreatedDate( appUser.getCreatedDate() );
        userResponseDTO.setLastModifiedDate( appUser.getLastModifiedDate() );
        userResponseDTO.setFullname( appUser.getFullname() );
        userResponseDTO.setUsername( appUser.getUsername() );
        userResponseDTO.setPassword( appUser.getPassword() );
        Collection<Role> collection = appUser.getRoles();
        if ( collection != null ) {
            userResponseDTO.setRoles( new ArrayList<Role>( collection ) );
        }

        return userResponseDTO;
    }

    @Override
    public List<UserResponseDTO> toUserResponseDTOList(List<AppUser> appUserList) {
        if ( appUserList == null ) {
            return null;
        }

        List<UserResponseDTO> list = new ArrayList<UserResponseDTO>( appUserList.size() );
        for ( AppUser appUser : appUserList ) {
            list.add( toUserResponseDTO( appUser ) );
        }

        return list;
    }
}
