package com.sina.khanoo.zone;

import com.sina.khanoo.city.City;
import com.sina.khanoo.city.CityRequestDTO;
import com.sina.khanoo.city.CityResponseDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-20T01:41:08+0430",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.1 (Oracle Corporation)"
)
@Component
public class ZoneMapperImpl implements ZoneMapper {

    @Override
    public Zone toZone(ZoneRequestDTO zoneRequestDTO) {
        if ( zoneRequestDTO == null ) {
            return null;
        }

        Zone zone = new Zone();

        zone.setCity( cityRequestDTOToCity( zoneRequestDTO.getCityRequestDTO() ) );
        zone.setId( zoneRequestDTO.getId() );
        zone.setName( zoneRequestDTO.getName() );

        return zone;
    }

    @Override
    public List<Zone> toZoneList(List<ZoneRequestDTO> zoneRequestDTOList) {
        if ( zoneRequestDTOList == null ) {
            return null;
        }

        List<Zone> list = new ArrayList<Zone>( zoneRequestDTOList.size() );
        for ( ZoneRequestDTO zoneRequestDTO : zoneRequestDTOList ) {
            list.add( toZone( zoneRequestDTO ) );
        }

        return list;
    }

    @Override
    public ZoneResponseDTO toZoneResponseDTO(Zone zone) {
        if ( zone == null ) {
            return null;
        }

        ZoneResponseDTO zoneResponseDTO = new ZoneResponseDTO();

        zoneResponseDTO.setCityResponseDTO( cityToCityResponseDTO( zone.getCity() ) );
        zoneResponseDTO.setId( zone.getId() );
        zoneResponseDTO.setVersion( zone.getVersion() );
        zoneResponseDTO.setCreatedDate( zone.getCreatedDate() );
        zoneResponseDTO.setLastModifiedDate( zone.getLastModifiedDate() );
        zoneResponseDTO.setName( zone.getName() );
        zoneResponseDTO.setIsActive( zone.getIsActive() );

        return zoneResponseDTO;
    }

    @Override
    public List<ZoneResponseDTO> toZoneResponseDTOList(List<Zone> zoneList) {
        if ( zoneList == null ) {
            return null;
        }

        List<ZoneResponseDTO> list = new ArrayList<ZoneResponseDTO>( zoneList.size() );
        for ( Zone zone : zoneList ) {
            list.add( toZoneResponseDTO( zone ) );
        }

        return list;
    }

    protected City cityRequestDTOToCity(CityRequestDTO cityRequestDTO) {
        if ( cityRequestDTO == null ) {
            return null;
        }

        City city = new City();

        city.setId( cityRequestDTO.getId() );
        city.setName( cityRequestDTO.getName() );

        return city;
    }

    protected CityResponseDTO cityToCityResponseDTO(City city) {
        if ( city == null ) {
            return null;
        }

        CityResponseDTO cityResponseDTO = new CityResponseDTO();

        cityResponseDTO.setId( city.getId() );
        cityResponseDTO.setVersion( city.getVersion() );
        cityResponseDTO.setCreatedDate( city.getCreatedDate() );
        cityResponseDTO.setLastModifiedDate( city.getLastModifiedDate() );
        cityResponseDTO.setName( city.getName() );
        cityResponseDTO.setIsActive( city.getIsActive() );

        return cityResponseDTO;
    }
}
